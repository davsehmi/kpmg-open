import React, { Component } from 'react';
import { getIndexInArray } from '../../utils/helpers';
import ArticleLong from '../../components/article/ArticleLong';
import ArticleFullPage from '../../components/article/ArticleFullPage';
import Footer from '../../components/footer/Footer';
import './article.scss';

class Article extends Component {
	constructor(props) {
    super(props);
    this.articleIndex = this.articleIndex.bind(this);
  }

  articleIndex() {
  	const { data, match } = this.props;
    return getIndexInArray(match.params.article_slug, data.articles);
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  componentDidUpdate(prevProps) {
    if ( prevProps.match.url !== this.props.match.url) {
      window.scrollTo(0, 0);
    }
  }




  render() {
    const { articles } = this.props.data;
    const { dimensions } = this.props;
    const index = this.articleIndex();
  	const article = this.props.data.articles[index];
    return (
      <React.Fragment>
        {article.article_layout === 'Layout A' &&
          <ArticleLong article_index={index} articles={articles} article={article} dimensions={dimensions} />
        }
        {article.article_layout === 'full_page' &&
          <ArticleFullPage article_index={index} articles={articles} article={article} dimensions={dimensions} />
        }
        <Footer />
      </React.Fragment>
    );
  }
}

export default Article;
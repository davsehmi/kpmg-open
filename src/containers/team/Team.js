import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import HeroSmall from '../../components/hero/HeroSmall';
import PostAnimation from '../../components/post/PostAnimation';
import Image from '../../components/image/Image';
import Hr from '../../components/misc/Hr';
import Footer from '../../components/footer/Footer'

class Team extends Component {
	render() {
		const { authors } = this.props.data;
		return (
			<React.Fragment>
				<HeroSmall
					title="Meet the team"
					caption="KPMG Open"
				/>
				<div className="team">
					<div className="container-fluid">
						<div className="row">

							{authors.map((author, i) => (
								<div className={(i % 2 === 0 && 'col-md-offset-1') + ' col-md-3'} key={i}>
									<PostAnimation>
										<div className="post">
							        <Hr color="#483699" />
							        <Image
									  		url={author.image}
									  		placeholder={author.image_tiny}
									  		width="100%"
									  		ratio="56.25%"
									  	/>
										  <div className={`post__content post__${author.slug}`}>
							          <div className="post__title"><span>{author.title}</span></div>
							          <div className="post__excerpt" dangerouslySetInnerHTML={{__html: author.content}} />
								      </div>
							      </div>
							    </PostAnimation>
						    </div>
						  ))}

							<div className="col-md-offset-1 col-sm-6" style={{marginBottom: '60px'}}>
								<div className="post__title"><span>Got a story to tell?</span></div>
								<div className="post__excerpt"><p>Contact the team <Link to="/say-hello">here</Link></p></div>
							</div>
						</div>
					</div>
				</div>
				<Footer />
			</React.Fragment>
		);
	}
}

export default Team;

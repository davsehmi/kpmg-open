import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { getIndexInArray, determineColor } from '../../utils/helpers';
import ScrollMagic from 'scrollmagic';
import Intro from '../../components/intro/Intro';
import PostPreviewLarge from '../../components/post/PostPreviewLarge';
import PostPreviewMedium from '../../components/post/PostPreviewMedium';
import PostPreviewSmall from '../../components/post/PostPreviewSmall';
import Header from '../../components/header/Header';
import Hero from '../../components/hero/Hero';
import Footer from '../../components/footer/Footer';
import './home.scss';

let sceneSidebar = null;

class Home extends Component {
  constructor(props) {
    super(props);
    this.controller = new ScrollMagic.Controller();
    // this.navAnimation = this.navAnimation.bind(this);
    this.issueIndex = this.issueIndex.bind(this);
    this.content = null;
    this.setContent = (element) => {
      this.textInput = element;
    }
  }

  issueIndex() {
    const { data, match } = this.props;
    return getIndexInArray(match.params.issue_slug, data.issues);
  }

  // navAnimation() {
  //   const { navOn, navOff, closeSidebar, openSidebar } = this.props;

  //   sceneSidebar = new ScrollMagic.Scene({triggerElement: this.setContent, duration: 0, offset: -300})
  //     .on('enter', function(event) {
  //       if (event.scrollDirection === 'FORWARD') {
  //         navOn();
  //         closeSidebar();
  //       }
  //     })
  //     .on('leave', function(event) {
  //       if (event.scrollDirection === 'REVERSE') {
  //         navOff();
  //         openSidebar();
  //       }
  //     })
  //     .addTo(this.controller);
  // }

  componentDidMount() {
    // this.navAnimation();
    // window.scrollTo(0, 0);
    // this.props.openSidebar();
  }

  render() {
    const { match, navOff, openSidebar, introPlayed, intro, dimensions } = this.props;
    const current_issue = this.props.data.issues[this.issueIndex()];
    return (
      <React.Fragment>

        {current_issue
          ? <div>
              {intro === false &&
                <Intro introPlayed={introPlayed} navOff={navOff} openSidebar={openSidebar} />
              }
              <Hero
                image={current_issue.hero_image}
                image_tiny={current_issue.hero_image_tiny}
                image_mobile={current_issue.hero_image_mobile}
                image_mobile_tiny={current_issue.hero_image_mobile_tiny}
                window_width={dimensions.width}
              />
              <Header />
              <section className="content" ref={div => this.setContent = div}>
                <div className="container-fluid">
                  <div className="row">

                    {current_issue.issue_layout.articles.map((article, i) => (

                      <React.Fragment key={article.id}>
                        {article.homepage_template === 'grid-half-width' &&
                          <PostPreviewMedium
                            article={article}
                            color={determineColor(article.category.slug)}
                            issue_slug={match.params.issue_slug}
                            window_width={dimensions.width}
                          />
                        }
                        {article.homepage_template === 'full-width-image-left' &&
                          <PostPreviewLarge
                            article={article}
                            color={determineColor(article.category.slug)}
                            issue_slug={match.params.issue_slug}
                            window_width={dimensions.width}
                            align="left"
                          />
                        }
                        {article.homepage_template === 'full-width-image-right' &&
                          <PostPreviewLarge
                            article={article}
                            color={determineColor(article.category.slug)}
                            issue_slug={match.params.issue_slug}
                            window_width={dimensions.width}
                            align="right"
                          />
                        }
                        {article.homepage_template === 'grid-third-width' &&
                          <PostPreviewSmall
                            article={article}
                            color={determineColor(article.category.slug)}
                            issue_slug={match.params.issue_slug}
                            window_width={dimensions.width}
                          />
                        }
                      </React.Fragment>

                    ))}

                  </div>
                </div>
              </section>
              <Footer />
            </div>

          : <Redirect to="/issue/spring-2019" />
        }

      </React.Fragment>
    );
  }
}

export default Home;
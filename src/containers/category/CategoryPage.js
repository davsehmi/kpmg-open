import React, { Component } from 'react';
import { getIndexInArray, determineColor } from '../../utils/helpers';
import HeroSmall from '../../components/hero/HeroSmall';
import Footer from '../../components/footer/Footer';
import PostPreviewMedium from '../../components/post/PostPreviewMedium';

class CategoryPage extends Component {
	constructor(props){
    super(props);
    this.categoryIndex = this.categoryIndex.bind(this);
    this.handleCategoryChange = this.handleCategoryChange.bind(this);
    this.resetArticles = this.resetArticles.bind(this);
    this.state = {
    	display_articles: []
    }
  }

  categoryIndex() {
  	const { data, match } = this.props;
    return getIndexInArray(match.params.category_slug, data.categories);
  }

  handleCategoryChange() {
  	const { data, match } = this.props;

    const issue = data.issues.filter(function(item) {
		  return item.slug === match.params.issue_slug;
		});

		const display_articles = issue[0].issue_layout.articles.filter(function(item) {
			return item.category.slug === match.params.category_slug;
		});

		console.log(display_articles);

		this.setState({
			display_articles
		})
  }

  resetArticles() {
  	this.setState({
    	display_articles: []
    })
  }

  componentDidMount() {
  	window.scrollTo(0, 0);
  	this.resetArticles();
  	this.handleCategoryChange();
  }

  componentDidUpdate(prevProps) {
   	if ( prevProps.match.params.category_slug !== this.props.match.params.category_slug) {
   		window.scrollTo(0, 0);
   		this.resetArticles();
   		setTimeout(function() {
        this.handleCategoryChange();
    	}.bind(this), 100);
   	}
  }

	render() {
		const { display_articles } = this.state;
		const category = this.props.data.categories[this.categoryIndex()];
		return (
			<React.Fragment>
				{category
					? <React.Fragment>
							<HeroSmall
								title={category.title}
								caption="Category"
								text_color="#fff"
								bg_color={determineColor(category.slug)}
							/>
								<div className="container-fluid">
								  <div className="row">
										{display_articles.length > 0 &&
											display_articles.map((display_article, i) => (
												<PostPreviewMedium
									        article={display_article}
									        color={determineColor(display_article.category.slug)}
									        issue_slug={display_article.issue_slug}
									        featured={false}
									        offset={i % 2 === 0 ? true : false}
									      />
										))}
									</div>
								</div>
								<Footer />
							</React.Fragment>
					: 'Category does not exist'
				}
			</React.Fragment>
		);
	}
}

export default CategoryPage;


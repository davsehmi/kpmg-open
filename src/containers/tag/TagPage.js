import React, { Component } from 'react';
import { getIndexInArray } from '../../utils/helpers';
import { determineColor } from '../../utils/helpers';
import HeroSmall from '../../components/hero/HeroSmall';
import Footer from '../../components/footer/Footer';
import PostPreviewMedium from '../../components/post/PostPreviewMedium';

class TagPage extends Component {
	constructor(props){
    super(props);
    this.tagIndex = this.tagIndex.bind(this);
    this.handleTagChange = this.handleTagChange.bind(this);
    this.resetArticles = this.resetArticles.bind(this);
    this.state = {
    	display_articles: [],
    }
  }

  tagIndex() {
  	const { data, match } = this.props;
    return getIndexInArray(match.params.tag_slug, data.tags);
  }

  handleTagChange() {
  	const { data, match } = this.props;

    const issue = data.issues.filter(function(item) {
		  return item.slug === match.params.issue_slug;
		});

		const display_articles = issue[0].issue_layout.articles.filter(function(item) {
			return item.tags.filter(function(tag) {
		  	if (tag.slug === match.params.tag_slug) {
		  		return item;
		  	}
			});
		});

		this.setState({
			display_articles
		})
  }

  resetArticles() {
  	this.setState({
			display_articles: []
		})
  }

  componentDidMount() {
  	window.scrollTo(0, 0);
  	this.resetArticles();
  	this.handleTagChange();
  }

  componentDidUpdate(prevProps) {
   	if ( prevProps.match.params.tag_slug !== this.props.match.params.tag_slug) {
   		window.scrollTo(0, 0);
   		this.resetArticles();
   		setTimeout(function() {
        this.handleTagChange();
    	}.bind(this), 100);
   	}
  }

	render() {
		const { display_articles } = this.state;
		const tag = this.props.data.tags[this.tagIndex()];
		return (
			<React.Fragment>
				{tag
					? <React.Fragment>
							<HeroSmall title={tag.title} caption="Tag" />
								<div className="container-fluid">
								  <div className="row">
										{display_articles.length > 0 &&
											display_articles.map((display_article, i) => (
												<PostPreviewMedium
													key={i}
									        article={display_article}
									        color={determineColor(display_article.category.slug)}
									        issue_slug={display_article.issue_slug}
									        featured={false}
									        offset={i % 2 === 0 ? true : false}
									      />
										))}
									</div>
								</div>
								<Footer />
							</React.Fragment>
					: 'Tag does not exist'
				}
			</React.Fragment>
		);
	}
}

export default TagPage;

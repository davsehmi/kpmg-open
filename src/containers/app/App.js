import React, { Component } from 'react';
import { HashRouter as Router, Route } from "react-router-dom";
import Loading from '../../components/svgs/Loading';
import Routes from '../../routes/Routes';
import { getData } from '../../utils/helpers';
import './app.scss';

class App extends Component {
  constructor(props){
    super(props);
    this.onDataSuccess = this.onDataSuccess.bind(this);
    this.onDataFailure = this.onDataFailure.bind(this);
    this.updateDimensions = this.updateDimensions.bind(this);
    this.state = {
      data: {},
      fetching: true,
      dimensions: {
        width: null,
        height: null,
      }
    };
  }

  updateDimensions() {
    this.setState({
      dimensions: {
        width: window.innerWidth,
        height: window.innerHeight
      }
    })
  }

  onDataSuccess(response) {
    this.setState({
      data: response,
      fetching: false
    });
  }

  onDataFailure(error) {
    console.log(error);
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);

    const url = function() {
      if (process.env.NODE_ENV === 'development') {
        return 'http://localhost:3001/ss';
      }
      if (process.env.NODE_ENV === 'staging') {
        return 'http://open.kpmg.abcomm.co.uk/ss';
      }
      if (process.env.NODE_ENV === 'production') {
        return 'https://portal.kpmg.co.uk/kpmglife/onlinemagazine/ss/data.txt';
      }
    }
    getData(url(), this.onDataSuccess, this.onDataFailure );
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);
  }

  render() {
    const { fetching } = this.state;
    return (
      <React.Fragment>
        {fetching ?
          <div className="loading-screen">
            <Loading size="60" />
          </div>
          :
          <Router>
            <Route render={routeProps => <Routes {...routeProps} {...this.state} />} />
          </Router>
        }
      </React.Fragment>
    )
  }
}

export default App;

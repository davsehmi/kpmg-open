import React from 'react';
import ReactDOM from 'react-dom';
import 'core-js';
import './styles/styles.scss';
import App from './containers/app/App';
import './index.scss';

ReactDOM.render(<App />, document.getElementById('root'));
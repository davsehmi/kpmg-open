import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Home from '../containers/home/Home';
import Article from '../containers/article/Article';
import CategoryPage from '../containers/category/CategoryPage';
import TagPage from '../containers/tag/TagPage';
import Sidebar from '../components/sidebar/Sidebar';
import Team from '../containers/team/Team';
import Contact from '../containers/contact/Contact';


class Routes extends Component {
  constructor(props){
    super(props);
    this.handleSidebarToggle = this.handleSidebarToggle.bind(this);
    this.closeSidebar = this.closeSidebar.bind(this);
    this.openSidebar = this.openSidebar.bind(this);
    this.navOff = this.navOff.bind(this);
    this.navOn = this.navOn.bind(this);
    this.introPlayed = this.introPlayed.bind(this);
    this.state = {
      is_sidebar_nav: true,
      is_sidebar_open: false,
      intro_played: false,
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.setState({
        is_sidebar_nav: true,
        is_sidebar_open: false,
      });
    }
  }

  introPlayed() {
    this.setState({ intro_played: true });
  }

  closeSidebar() {
    this.setState({ is_sidebar_open: false });
  }

  openSidebar() {
    this.setState({ is_sidebar_open: true });
  }

  navOff() {
    this.setState({ is_sidebar_nav: false });
  }

  navOn() {
    this.setState({ is_sidebar_nav: true });
  }

  handleSidebarToggle() {
    if (this.state.is_sidebar_open) {
      this.closeSidebar();
    }
    else {
      this.openSidebar();
    }
  }

  render() {
    const { data, dimensions } = this.props;
    const { is_sidebar_nav, is_sidebar_open, intro_played } = this.state;
    // <Redirect exact from='/' to={`/issue/issue-${data.issues.length}`}/>
    // <Route path="/issue/:issue_slug" render={routeProps => <Home {...routeProps} {...data}/>}/>
    return (
      <React.Fragment>

        <Switch>
          <Redirect push exact from='/' to={`/issue/${data.issues[data.issues.length-1].slug}`}/>
        </Switch>

        <Route path="/" render={routeProps =>
          <Sidebar
            is_sidebar_nav={is_sidebar_nav}
            handleSidebarToggle={this.handleSidebarToggle}
            closeSidebar={this.closeSidebar}
            is_sidebar_open={is_sidebar_open}
            data={data}
            dimensions={dimensions}
            {...routeProps}
          />
        }/>

        <Switch>
          <Route path="/issue/:issue_slug/article/:article_slug" render={routeProps =>
            <Article
              data={data}
              dimensions={dimensions}
              {...routeProps}
            />
          }/>
          <Route path="/issue/:issue_slug/category/:category_slug" render={routeProps =>
            <CategoryPage
              data={data}
              dimensions={dimensions}
              {...routeProps}
            />
          }/>
          <Route path="/issue/:issue_slug/tag/:tag_slug" render={routeProps =>
            <TagPage
              data={data}
              dimensions={dimensions}
              {...routeProps}
            />
          }/>
          <Route path="/meet-the-team" render={routeProps =>
            <Team
              data={data}
              dimensions={dimensions}
              {...routeProps}
            />
          }/>
          <Route path="/say-hello" render={routeProps =>
            <Contact
              data={data}
              dimensions={dimensions}
              {...routeProps}
            />
          }/>
          <Route path="/issue/:issue_slug" render={routeProps =>
            <Home
              navOn={this.navOn}
              navOff={this.navOff}
              handleSidebarToggle={this.handleSidebarToggle}
              closeSidebar={this.closeSidebar}
              openSidebar={this.openSidebar}
              is_sidebar_nav={is_sidebar_nav}
              is_sidebar_open={is_sidebar_open}
              introPlayed={this.introPlayed}
              intro={intro_played}
              data={data}
              dimensions={dimensions}
              {...routeProps}
            />
          }/>
        </Switch>

      </React.Fragment>
    );
  }
}

export default Routes;
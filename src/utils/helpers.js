import axios from 'axios';

export function getData(url, onSuccess, onFailure) {
  axios.get(url)
    .then(response => {
      onSuccess(response.data);
    })
    .catch(error => {
      onFailure(error);
    });
}

export function getIndexInArray(nameKey, myArray, slug = 'slug') {
  for (var i=0; i < myArray.length; i++) {
    if (myArray[i][slug] === nameKey) {
        return i;
    }
  }
}

export function getImage(location) {
  if ( process.env.NODE_ENV === 'development' ) {
    return `http://localhost:3001${location}`;
  }
  if ( process.env.NODE_ENV === 'staging' ) {
    return `http://open.kpmg.abcomm.co.uk${location}`;
  }
  if ( process.env.NODE_ENV === 'production' ) {
    return `.${location}`;
  }
}

export function determineColor(category) {
  if (category === 'point-of-view') {
    return '#00AEA8';
  }
  if (category === 'briefing') {
    return '#005EB8';
  }
  if (category === 'in-depth') {
    return '#483698';
  }
}
import React, { Component } from 'react';
import './video.scss'
// https://cdnapisec.kaltura.com/p/2276841/sp/227684100/embedIframeJs/uiconf_id/39143382/partner_id/2276841
// partner id: 2276841, uiconf_id: 39143382

// let script =  null;

class Video extends Component {

  componentDidMount() {
  	// script = document.createElement('script');
    // script.src = 'https://cdnapisec.kaltura.com/p/2276841/sp/227684100/embedIframeJs/uiconf_id/39143382/partner_id/2276841';
    // document.body.appendChild(script);


		window.kWidget.embed({
		  'targetId': 'kaltura_player_1548863729',
		  'wid': '_2276841',
		  'uiconf_id': 39143422,
		  'flashvars': {},
		  'cache_st': 1548863729,
		  'entry_id': '0_98qck8no'
		});
  }

  componentWillUnmount() {
  	window.kWidget.destroy('kaltura_player_1548863729');
		// document.body.removeChild(script);
  }

	render() {
		return (
			<React.Fragment>
				<div id="kaltura_player_1548863729" className="kaltura_player__embed"></div>
				<div className="kaltura_player__embed">
					<iframe id="kmsembed-0_98qck8no" src="https://kpmguk.mediaspace.kaltura.com/embed/secure/iframe/entryId/0_98qck8no/uiConfId/39143422" allowfullscreen webkitallowfullscreen mozAllowFullScreen allow="autoplay *; fullscreen *; encrypted-media *" frameborder="0" title="Kaltura Player"></iframe>
				</div>
			</React.Fragment>
		);
	}
}

export default Video;

import React, { Component } from 'react';
import Caption from '../post/Caption';
import './hero.scss';

class HeroSmall extends Component {
	render() {
		const { title, caption, text_color, bg_color } = this.props;

		return (
			<section style={{color: text_color ? text_color : 'initial', background: bg_color ? bg_color : '#f2f2f2' }} className="hero-small">
				<div className="container-fluid">
					<div className="row">
						<div className="col-sm-offset-1 col-sm-8">
							<Caption text={caption.toUpperCase()} />
							<div className="hero-small__title">{title}</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default HeroSmall;
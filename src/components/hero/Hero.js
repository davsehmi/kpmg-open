import React, { Component } from 'react';
import { getImage } from '../../utils/helpers';
import ProgressiveImage from '../image/ProgressiveImage';
import PostAnimation from '../post/PostAnimation';
import './hero.scss';

class Hero extends Component {
	render() {
		const { image, image_tiny, image_mobile, image_mobile_tiny, title, author, content, window_width, category } = this.props;
		return (
			<section className="hero">
				<ProgressiveImage
					url={getImage(image)}
					placeholder={getImage(image_tiny)}
					url_mobile={getImage(image_mobile)}
					placeholder_mobile={getImage(image_mobile_tiny)}
					width="100%"
					height="100vh"
					window_width={window_width}
				>
					{title &&
						<div className={(category && `hero__children--${category} `) + 'hero__children'}>
							<div className="container">
			          <div className="row">
				          <div className="col-sm-8">
					          <PostAnimation offset={-300}>
				          		<div className="hero__text hero__title"><span>{title}</span></div>
				          	</PostAnimation>
				          	{author &&
				          		<PostAnimation offset={-300}>
				          			<div className="hero__text hero__author"><span>By {author}</span></div>
				          		</PostAnimation>
				          	}
				          	<PostAnimation offset={-300}>
				          		<div className="hero__text hero__content" dangerouslySetInnerHTML={{__html: content}} />
				          	</PostAnimation>
				          </div>
			          </div>
			        </div>
			      </div>
		    	}

				</ProgressiveImage>
			</section>
		);
	}
}

export default Hero;

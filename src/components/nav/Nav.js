import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './nav.scss';

class Nav extends Component {

  render() {
    const { issue_slug } = this.props;
    return (
      <nav>
        <ul className='nav'>
          <li><Link to={`/issue/${issue_slug}`}>Home</Link></li>
          <li><Link to={`/issue/${issue_slug}/category/briefing`}>Briefing</Link></li>
          <li><Link to={`/issue/${issue_slug}/category/point-of-view`}>Point of view</Link></li>
          <li><Link to={`/issue/${issue_slug}/category/in-depth`}>In depth</Link></li>
          <li><Link to="/meet-the-team">Meet the team</Link></li>
          <li><Link to="/say-hello">Say hello</Link></li>
          <li><a href="https://portal.kpmg.co.uk" target="_blank" rel="noopener noreferrer">UK Portal</a></li>
        </ul>
      </nav>
    );
  }
}

export default Nav;
import React, { Component } from 'react';
import KPMGLogo from '../svgs/KPMGLogo';
import './footer.scss';

class Footer extends Component {
	render() {
		return (
			<section className="footer">
        <div className="copy">
        	<div className="footer__logo">
						<KPMGLogo width="100" height="40" />
					</div>
          <p>&copy; 2019 KPMG International Cooperative (“KPMG International”), a Swiss entity. Member firms of the KPMG network of independent firms are affiliated with KPMG International. KPMG International provides no client services. No member firm has any authority to obligate or bind KPMG International or any other member firm vis-à-vis third parties, nor does KPMG International have any such authority to obligate or bind any member firm. All rights reserved.</p>
        </div>
      </section>
		);
	}
}

export default Footer;

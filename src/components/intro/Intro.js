import React, { Component } from 'react';
import { TimelineLite, Power3} from "gsap/TweenMax";
import './intro.scss';

class Intro extends Component {
  constructor(props){
    super(props);
    this.introTransition1 = null;
    this.introTransition2 = null;
    this.introTransition3 = null;
    this.introWrapper = null;
    this.introTween = new TimelineLite({paused: true});
  }

  componentDidMount(){
    this.introTween
      // .add(this.props.navOff, '0')

      .set('body', {overflow: 'hidden'}, '0')

      .to(this.introTransition1, 0.4, {ease:  Power3.easeInOut, transform: 'scaleX(1)'}, '0')
      .to(this.introTransition1, 0, {transformOrigin: '100% 100%'}, '0.4')
      .to(this.introTransition1, 0.4, {ease:  Power3.easeInOut, transform: 'scaleX(0)'}, '0.4')

      .to(this.introTransition2, 0.4, {ease:  Power3.easeInOut, transform: 'scaleX(1)'}, '0.4')
      .to(this.introTransition2, 0, {transformOrigin: '100% 100%'}, '0.8')
      .to(this.introTransition2, 0.4, {ease:  Power3.easeInOut, transform: 'scaleX(0)'}, '0.8')

      // .add(this.props.openSidebar, '0.5')

      .to(this.introWrapper, 0, {background: 'transparent'}, '0.8')
      .to(this.introWrapper, 0, {display: 'none'}, '1.2')

      .set('body', {overflow: 'initial'}, '1.2')

      .add( this.props.introPlayed, '1.2')

      .play();
  }

  render() {
    return (
      <div className="intro__wrapper" ref={div => this.introWrapper = div}>
        <div className="intro__transition intro__transition--1" ref={div => this.introTransition1 = div}></div>
        <div className="intro__transition intro__transition--2" ref={div => this.introTransition2 = div}></div>
      </div>
    );
  }
}

export default Intro;
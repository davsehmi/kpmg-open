import React, { Component } from 'react';
import './error.scss';

class Error extends Component {
	render() {
		const { message } = this.props;
		return (
			<div className="error-page">
				{message}
			</div>
		);
	}
}

export default Error;

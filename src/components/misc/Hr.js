import React, { Component } from 'react';

class Hr extends Component {
	render() {
		const { marginTop } = this.props || 0;
		const { height } = this.props || 4;
		const { color } = this.props || '#000';
		const style = {
			marginTop,
			height,
			background: color
		}
		return (
			<div style={style} className="post__hr"></div>
		);
	}
}

export default Hr;

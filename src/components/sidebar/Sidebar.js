import React, { Component } from 'react';
import { getIndexInArray } from '../../utils/helpers';
import Nav from '../nav/Nav';
import Search from '../search/Search';
import KPMGLogo from '../svgs/KPMGLogo';
import Hamburger from './Hamburger';
import SearchIcon from '../svgs/SearchIcon';
import './sidebar.scss';

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.issueIndex = this.issueIndex.bind(this);
    this.handleSearchToggle = this.handleSearchToggle.bind(this);
    this.handleSidebarWithSearch = this.handleSidebarWithSearch.bind(this);
    this.state = {
      is_search_open: false,
    }
  }

  issueIndex() {
    const { data, match } = this.props;
    return getIndexInArray(match.params.issue_slug, data.issues);
  }

  handleSearchToggle() {
    if (this.state.is_search_open) {
      this.setState({ is_search_open: false });
    }
    else {
      this.props.closeSidebar();
      this.setState({ is_search_open: true });
    }
  }

  handleSidebarWithSearch() {
    this.props.handleSidebarToggle();
    if ( this.state.is_search_open ) {
      this.setState({ is_search_open: false });
    }
  }

  render() {
    const { is_sidebar_nav, is_sidebar_open, data, history } = this.props;
    const current_issue = this.props.data.issues[this.issueIndex()] ||  this.props.data.issues[this.props.data.issues.length - 1];
    const { is_search_open } = this.state;
    return (
      <React.Fragment>

          <header className={'sidebar' + (is_sidebar_open ? ' open' : '') + (is_sidebar_nav ? ' nav' : '')}>
            <div className={'sidebar__content ' + (is_sidebar_nav ? 'on' : '')}>
              <div className="sidebar__nav">
                <div className="sidebar__toggle"><Hamburger handleSidebarToggle={this.handleSidebarWithSearch} is_sidebar_open={is_sidebar_open} /></div>
                <div className="search__toggle" onClick={this.handleSearchToggle}><SearchIcon /></div>
                <Nav is_sidebar_nav={this.props.is_sidebar_nav} issue_slug={current_issue.slug} />
              </div>
            </div>
          </header>

          <div className="mobile-button">
            <Hamburger handleSidebarToggle={this.handleSidebarWithSearch} is_sidebar_open={is_sidebar_open} />
          </div>

          <Search history={history} handleSearchToggle={this.handleSearchToggle} is_search_open={is_search_open} articles={data.articles} />

      </React.Fragment>
    );
  }
}

export default Sidebar;
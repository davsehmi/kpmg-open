import React, { Component } from 'react';
import './hamburger.scss';

class Hamburger extends Component {

  render() {
    const { is_sidebar_open, handleSidebarToggle } = this.props;
    return (
      <div className={'hamburger hamburger--spin' + (is_sidebar_open ? ' is-active' : '')} onClick={handleSidebarToggle}>
        <div className="hamburger-box">
          <div className="hamburger-inner"></div>
        </div>
      </div>
    );
  }
}

export default Hamburger;
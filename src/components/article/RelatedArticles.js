import React, { Component } from 'react';
import PostContent from '../post/PostContent';
import './relatedArticles.scss';


class RelatedArticles extends Component {
	render() {
		const { articles, article_index } = this.props;
		const prev_article = article_index === 0 ? articles[articles.length - 1] : articles[article_index - 1];
		const next_article = article_index === (articles.length - 1) ? articles[0] : articles[article_index + 1];

		return (
			<div className="related">
				<div className="related__block related__prev">
				  <div className="related__link">Previous article</div>
				  <PostContent article={prev_article} issue_slug={prev_article.issue_slug} color="#fff" />
				</div>
				<div className="related__block related__next">
				  <div className="related__link">Next article</div>
				  <PostContent article={next_article} issue_slug="" color="#fff" />
				</div>
			</div>

		);
	}
}

export default RelatedArticles;

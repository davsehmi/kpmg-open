import React, { Component } from 'react';
import PostAnimation from '../post/PostAnimation';
import './infographic.scss';

class Infographic extends Component {
	render() {
		const { images } = this.props;
		return (
			<div className="container">
        <div className="row">
          <div className="col-sm-8">
            <div className="infographic">
              <PostAnimation>
            	 <div className="infographic__spacer"></div>
              </PostAnimation>
          		{images.map((image, i) => (
          			<React.Fragment key={i}>
                  <PostAnimation>
  	          			<img className="infographic__image" src={image.image} alt=""/>
                  </PostAnimation>
                  <PostAnimation>
  	          			<div className="infographic__spacer"></div>
                  </PostAnimation>
          			</React.Fragment>
        			))}
            </div>
          </div>
        </div>
      </div>
		);
	}
}

export default Infographic;

import React, { Component } from 'react';
import Hero from '../hero/Hero';
import Video from '../video/Video';
import RelatedArticles from './RelatedArticles';
import Infographic from './Infographic';
import PostAnimation from '../post/PostAnimation';
import ScrollMagic from 'scrollmagic';
import './articles.scss';

let sceneArticle = null;

class ArticleLong extends Component {

  constructor(props) {
    super(props);
    this.controller = new ScrollMagic.Controller();
    this.setScene = this.setScene.bind(this);
    this.state = {
      active: false
    };
  }

  setScene() {

    const _this = this;

    const quotes = document.querySelectorAll('.article__content blockquote');
    const images = document.querySelectorAll('p.right');
    const imagesCenter = document.querySelectorAll('p img.leftAlone');

    if ( quotes.length > 0 ) {
      quotes.forEach(function(quote){
        sceneArticle = new ScrollMagic.Scene({triggerElement: quote, duration: 0, offset: _this.props.offset ? _this.props.offset : -100})
          .on('enter', function(event) {
            quote.classList.add('active');
          })
          .addTo(_this.controller);
      });
    }
    if ( images.length > 0 ) {
      images.forEach(function(image){
        sceneArticle = new ScrollMagic.Scene({triggerElement: image, duration: 0, offset: _this.props.offset ? _this.props.offset : -100})
          .on('enter', function(event) {
            image.classList.add('active');
          })
          .addTo(_this.controller);
      });
    }
    if ( imagesCenter.length > 0 ) {
      imagesCenter.forEach(function(imageCenter){
        sceneArticle = new ScrollMagic.Scene({triggerElement: imageCenter, duration: 0, offset: _this.props.offset ? _this.props.offset : -100})
          .on('enter', function(event) {
            imageCenter.classList.add('active');
          })
          .addTo(_this.controller);
      });
    }
  }

  componentDidMount() {
    this.setScene();
  }

  componentDidUpdate(prevProps) {
    if ( prevProps !== this.props ) {
      sceneArticle && sceneArticle.remove();
      this.setScene();
    }
  }

  componentWillUnmount() {
    sceneArticle && sceneArticle.remove();
  }

	render() {
		const { article, article_index, articles, window_width } = this.props;

		return (
      <React.Fragment>

        <Hero
          image={article.hero_image}
          image_tiny={article.hero_image_tiny}
          image_mobile={article.hero_image_mobile}
          image_mobile_tiny={article.hero_image_mobile_tiny}
          window_width={window_width}
          title={article.title}
          author={article.author && article.author.title}
          content={article.hero_content}
          category={article.category.slug}
        />

        <div className="article__wrapper">

          {article.content_blocks.map((block, i) => (
            <React.Fragment key={i}>

                {block.content &&
                  <div className="container">
                    <div className="row">
                      <div className="col-sm-8">
                        <PostAnimation>
                          <div className="article">
                            <div className={`article__content article__content--${article.category.slug}`} dangerouslySetInnerHTML={{__html: block.content}} />
                          </div>
                        </PostAnimation>
                      </div>
                    </div>
                  </div>
                }

                {block.images.length > 0 && block.image_display === 'Single image' &&
                  <PostAnimation>
                    <Hero
                      image={block.images[0].image}
                      image_tiny={block.images[0].image_tiny}
                      image_mobile={block.images[0].image_mobile}
                      image_mobile_tiny={block.images[0].image_mobile_tiny}
                      window_width={window_width}
                    />
                  </PostAnimation>
                }

                {block.images.length > 0 && block.image_display === 'Infographic' &&
                  <Infographic images={block.images} />
                }

                {block.image_display === 'Video' &&
                  <PostAnimation>
                    <Video video_id={block.kaltura_video_id} />
                  </PostAnimation>
                }

            </React.Fragment>
          ))}

        </div>
        <RelatedArticles articles={articles} article_index={article_index} />
      </React.Fragment>
		);
	}
}

export default ArticleLong;

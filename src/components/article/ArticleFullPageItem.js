import React, { Component } from 'react';
import './articleFullPageItem.scss';

class ArticleFullPageItem extends Component {
	render() {
		const { number, item } = this.props;
		const style = {
			backgroundColor: `${item.bg_colour_from}`,
			background: `linear-gradient(135deg, ${item.bg_colour_from} 0%,${item.bg_colour_to} 100%)`,
			filter: `progid:DXImageTransform.Microsoft.gradient( startColorstr='${item.bg_colour_from}', endColorstr='${item.bg_colour_to}',GradientType=1 )`,
		}
		return (
			<div className="section" style={style}>
				<div className="container-fluid">
					<div className="row">

						<div className="col-lg-offset-1 col-md-4 col-lg-3 flex-row-center">
							<div className="fullpage__header">
								<div className="fullpage__number">{number}.</div>
								<div className="fullpage__title">{item.title}</div>
							</div>
							<div className="fullpage__content" dangerouslySetInnerHTML={{__html: item.content}} />
						</div>

						<div className="col-md-4 col-lg-3">

							{item.images.length > 0 &&
								<img src={item.images[0].image} alt="" className="fullpage__image"/>
							}
						</div>

					</div>
				</div>
			</div>
		);
	}
}

export default ArticleFullPageItem;
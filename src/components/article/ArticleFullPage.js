import React, { Component } from 'react';
import ReactFullpage from '@fullpage/react-fullpage';
import ArticleFullPageItem from './ArticleFullPageItem';
import RelatedArticles from './RelatedArticles';
import Hero from '../hero/Hero';
import Footer from '../footer/Footer';
import './articleFullPage.scss';

class ArticleFullPage extends Component {

	render() {
		const { article, articles, article_index, window_width } = this.props;
		return (
		  <ReactFullpage
			  scrollingSpeed={800}
			  normalScrollElements={'.section-footer'}

		    render={({ state, fullpageApi }) => {
		      return (
		        <ReactFullpage.Wrapper>
		          <div className="section">
				        <Hero
				          image={article.hero_image}
				          image_tiny={article.hero_image_tiny}
				          image_mobile={article.hero_image_mobile}
				          image_mobile_tiny={article.hero_image_mobile_tiny}
				          window_width={window_width}
				          title={article.title}
				          author={article.author && article.author.title}
				          content={article.hero_content}
				          category={article.category.slug}
				        />
		          </div>
							{article.content_blocks.map((item, i) => (
								<React.Fragment>
									{i+1 <  article.content_blocks.length &&
										<ArticleFullPageItem key={i} number={i+1} item={item} />
									}
									{i+1 === article.content_blocks.length &&
										<div className="section fp-auto-height">
											<div className="article__wrapper">
												<div className="container">
			                    <div className="row">
			                      <div className="col-sm-8">
			                        <div className="article" style={{margin: '0'}}>
				                        <div className="row">
				                          <div className="col-sm-4">
																		<img src={item.images[0].image} alt="" className="img-fluid"/>
																	</div>
																	<div className="col-sm-4">
																		<div className="article__content" dangerouslySetInnerHTML={{__html: item.content}} />
																	</div>
			                          </div>
			                        </div>
			                      </div>
			                    </div>
			                  </div>
		                  </div>
	                  </div>
									}
								</React.Fragment>
							))}
							<div className="section fp-auto-height">
								<RelatedArticles articles={articles} article_index={article_index} />
							</div>
							<div className="section fp-auto-height">
								<Footer />
							</div>
		        </ReactFullpage.Wrapper>
		      );
		    }}
		  />
		);
	}
}

export default ArticleFullPage;

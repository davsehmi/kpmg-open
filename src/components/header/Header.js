import React, { Component } from 'react';
import KPMGLogo from '../svgs/KPMGLogo';
import './header.scss';

class Header extends Component {
	render() {
		return (
      <div className="hero__sidebar">
        <div className="sidebar__intro">
          <header>
            <h1 className="title">OPEN</h1>
            <div className="subtitle">Come on in</div>
          </header>
          <main>
            <div className="intro__content">These are our stories. Of who we are and where we’ve come from. Of what’s on our minds and what we’re curious about. It’s where we open up, share our lives and embrace the opportunities ahead.</div>
            <div className="issue">Spring 2019</div>
            <div className="kpmg-logo">
              <KPMGLogo width="100" height="41" />
            </div>
          </main>
        </div>
      </div>
		);
	}
}

export default Header;

import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class Tag extends Component {
	render() {
		const { name, color, issue_slug, slug } = this.props;
		return (
			<Link to={`/issue/${issue_slug}/tag/${slug}`}>
				<span style={{color}}>{name}</span>
			</Link>
		);
	}
}

export default Tag;

import React, { Component } from 'react';
import Tag from './Tag';

class Tags extends Component {
	render() {
		const { tags, color, issue_slug } = this.props;
		return (
			<div className="post__tags">
				{tags.map((tag, i) => (
					<Tag key={i} name={`+ ${tag.title}`} slug={tag.slug} color={color} issue_slug={issue_slug} />
				))}
			</div>
		);
	}
}

export default Tags;

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Caption from './Caption';
import Hr from '../misc/Hr';
import Image from '../image/Image';
import PostContent from '../../components/post/PostContent';
import PostAnimation from './PostAnimation';

class PostPreviewSmall extends Component {
	render() {
		const { article, color, issue_slug, window_width } = this.props;
		return (
			<div className={(article.homepage_template_offset_required ? 'col-sm-offset-1 ' : '') + 'col-md-2'}>
				<PostAnimation>
					<div className="post">
						{article.is_featured_article &&
							<div className="post__featured"></div>
						}
		        <Hr color={color}/>
		        <div className="post__category post__category--pb">
					  	<Link to={`/issue/${article.issue_slug}/category/${article.category.slug}`}>
			          <Caption text={article.category.title.toUpperCase()} />
		          </Link>
		        </div>
		        <Link to={`/issue/${article.issue_slug}/article/${article.slug}`}>
				  		<Image
					  		url={article.thumbnail_image}
					  		placeholder={article.thumbnail_image_tiny}
					  		url_mobile={article.thumbnail_image_mobile}
					  		width="100%"
					  		ratio="56.25%"
					  		window_width={window_width}
					  	/>
				  	</Link>
		        <PostContent article={article} color={color} issue_slug={issue_slug} />
		      </div>
	      </PostAnimation>
	    </div>
		);
	}
}

export default PostPreviewSmall;




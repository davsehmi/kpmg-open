import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Caption from './Caption';
import Tags from './Tags';
import Hr from '../misc/Hr';
import './postContent.scss';

class PostContent extends Component {
  render() {
    const { article, color, issue_slug } = this.props;
    return (
      <div className={`post__content post__${article.category.slug}`}>
        <Link to={`/issue/${article.issue_slug}/article/${article.slug}`}>
          <div className="post__title"><span>{article.title}</span></div>
          <div className="post__excerpt">
            {article.homepage_introduction}
          </div>
        </Link>
        <Hr color={color} marginTop={20} height={1} />
        <Caption text={`${article.read_time} minute read`} />
        <Tags tags={article.tags} color={color} issue_slug={issue_slug} />
      </div>
    );
  }
}

export default PostContent;

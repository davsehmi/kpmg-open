import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Caption from './Caption';
import Hr from '../misc/Hr';
import Image from '../image/Image';
import PostContent from './PostContent';
import PostAnimation from './PostAnimation';

class PostPreviewMedium extends Component {
	render() {
		const { article, color, issue_slug, featured, offset, window_width } = this.props;
		// const is_featured = featured === true || featured === false ? featured : article.is_featured_article;
		const is_offset = offset === true || offset === false ? offset : article.homepage_template_offset_required;
		return (
			<div className={(is_offset ? 'col-md-offset-1 ' : '') + 'col-md-3'}>
				<PostAnimation>
					<div className="post">
						{featured === true || featured === false ? featured : article.is_featured_article &&
							<div className="post__featured"></div>
						}
		        <Hr color={color}/>
		        <div className="post__category post__category--pb">
					  	<Link to={`/issue/${article.issue_slug}/category/${article.category.slug}`}>
			          <Caption text={article.category.title.toUpperCase()} />
		          </Link>
		        </div>
		        <Link to={`/issue/${article.issue_slug}/article/${article.slug}`}>
			        <Image
					  		url={article.thumbnail_image}
					  		placeholder={article.thumbnail_image_tiny}
					  		url_mobile={article.thumbnail_image_mobile}
					  		width="100%"
					  		ratio="56.25%"
					  		window_width={window_width}
					  	/>
			      </Link>
		        <PostContent article={article} color={color} issue_slug={issue_slug} />
		      </div>
		    </PostAnimation>
	    </div>
		);
	}
}

export default PostPreviewMedium;

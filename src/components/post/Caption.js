import React, { Component } from 'react';

class Caption extends Component {
	render() {
		const { text, color } = this.props;
		const style = {
			color: color ? color : '#9B9B9B',
			fontSize: '1.2rem',
		}
		return (
			<div className="post__caption" style={style}>{text}</div>
		);
	}
}

export default Caption;

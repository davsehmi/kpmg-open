import React, { Component } from 'react';
import ScrollMagic from 'scrollmagic';
import './postAnimation.scss';

let scenePost = null;

class PostAnimation extends Component {

  constructor(props) {
    super(props);
    this.controller = new ScrollMagic.Controller();
    this.state = {
      active: false
    };
  }

  componentDidMount() {

  	const _this = this;

    scenePost = new ScrollMagic.Scene({triggerElement: this.post, duration: 0, offset: this.props.offset ? this.props.offset : -100})
      .on('enter', function(event) {
				_this.setState({
		      active: true
		    });
      })
      .addTo(this.controller);
  }

  componentWillUnmount() {
    scenePost.remove();
  }

	render() {
		const { active } = this.state;
		return (
			<div className={(active ? 'active ' : '') + 'post__animation'} ref={div => this.post = div}>
				{this.props.children}
			</div>
		);
	}
}

export default PostAnimation;

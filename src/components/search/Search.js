import React, { Component } from 'react';
import './search.scss';

let typingTimer;
let doneTypingInterval = 500;

class Search extends Component {
	constructor(props) {
		super(props);
		this.handleInputChange = this.handleInputChange.bind(this);
		this.getInfo = this.getInfo.bind(this);
		this.handleKeyDown = this.handleKeyDown.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.handleClickEvent = this.handleClickEvent.bind(this);
		this.state = {
			query: null,
			results: []
		}
	}

	getInfo() {
		const { articles } = this.props;
		const { query } = this.state;
		let results = [];

		if ( this.state.query && this.state.query.length > 2 ) {
			articles.forEach( function(article) {
				const fields_to_search = [article.title, article.homepage_introduction, article.first_paragraph, article.content];

				fields_to_search.forEach(function(field) {
					if (field && field.toLowerCase().includes(query.toLowerCase()) ) {
						results.push(article);
					}
				});
			});

			const unique_results = results.filter((v, i, a) => a.indexOf(v) === i);

			this.setState({
				results: unique_results.reverse(),
			});
		}
		else if ( this.state.query && this.state.query.length <= 2 ) {
			this.setState({
				results: [],
			});
		}
	}

	handleInputChange() {
   	this.setState({
     	query: this.search.value
   	});
 	}

 	handleKeyUp() {
		clearTimeout(typingTimer);
    typingTimer = setTimeout(this.getInfo, doneTypingInterval);
 	}

 	handleKeyDown() {
		clearTimeout(typingTimer);
 	}

 	handleClickEvent(path) {
 		this.props.handleSearchToggle();
 		this.props.history.push(path);
 	}

	render() {
		const { is_search_open, handleSearchToggle } = this.props;
		return (
		<div className={'search__wrapper ' + (is_search_open ? ' active ' : '') }>
      <form>
      	<div className="search__input">
	        <input
	          placeholder="Search..."
	          ref={input => this.search = input}
	          onChange={this.handleInputChange}
	          onKeyDown={this.handleKeyDown}
	          onKeyUp={this.handleKeyUp}
	        />
        	<div className="search__close" onClick={() => handleSearchToggle()}></div>
        </div>
        <ul className='search__results'>
	        {this.state.results.map((result, i) => (
						<li key={i} onClick={() => this.handleClickEvent(`/issue/${result.issue_slug}/article/${result.slug}`)}>
							{result.title}
							<span>{result.category.title}</span>
						</li>
					))}
				</ul>
      </form>
     </div>
		);
	}
}

export default Search;

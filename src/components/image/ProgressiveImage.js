import React, { Component } from 'react';
import './progressiveImage.scss';

class ProgressiveImage extends Component {
	constructor(props){
    super(props);
    this.handleImageLoaded = this.handleImageLoaded.bind(this);
    this.handleImageSize = this.handleImageSize.bind(this);
    this.imageLoaded = this.imageLoaded.bind(this);
    this.state = {
    	imageLoaded: false,
    	display_image: null,
    	display_image_tiny: null,
    }
  }

  handleImageLoaded() {
  	this.setState({
			imageLoaded: true
 		});
  }

  handleImageSize() {
  	const { url, placeholder, url_mobile, placeholder_mobile, window_width } = this.props;
  	if (window_width && window_width < 900) {
  		this.setState({
				display_image: url_mobile ? url_mobile : url,
    		display_image_tiny: placeholder_mobile ? placeholder_mobile : placeholder,
	 		});
  	}
  	else {
  		this.setState({
				display_image: url,
    		display_image_tiny: placeholder,
	 		});
  	}
  }

  loadImage(src) {
	  return new Promise((resolve, reject) => {
	    const image = new Image();
	    image.onload = () => resolve(src);
	    image.onerror = err => reject(err);
	    image.src = src;
	  });
	}

	imageLoaded() {
		this.setState({imageLoaded: true});
	}

	getImage(src) {
		const _this = this;
    this.loadImage(src)
      .then(function (src) {
				_this.setState({imageLoaded: true});
      })
      .catch(function (err) {
        console.log(err);
      });
		};

	componentWillMount() {
		this.handleImageSize();
 		setTimeout(function() {
      this.getImage(this.state.display_image);
  	}.bind(this), 100);
	}

	componentDidUpdate(prevProps) {
		if ( (prevProps.window_width < 900 && this.props.window_width >= 900) || (prevProps.window_width >= 900 && this.props.window_width < 900) ) {
			this.setState({imageLoaded: false});
			this.handleImageSize();
			this.getImage(this.state.display_image);
		}
		if (prevProps.url !== this.props.url) {
			this.setState({
				imageLoaded: false,
				display_image: null,
    		display_image_tiny: null
    	});
    	this.handleImageSize();
	 		setTimeout(function() {
	      this.getImage(this.state.display_image);
	  	}.bind(this), 100);
		}
	}

  render() {
  	const { imageLoaded, display_image, display_image_tiny } = this.state;
  	const { width, ratio, height, children, url, placeholder } = this.props;
  	const imageFixedHeight = {
			width: width,
			height: height,
			background: `url(${display_image}) 50% 50% / cover no-repeat`,
		}
		const imageFixedRatio = {
			width: width,
			paddingTop: ratio,
			background: `url(${display_image}) 50% 50% / cover no-repeat`,
		}
		const imageStyle = height ? imageFixedHeight : imageFixedRatio;

  	return (

			<div className="ph-image image" style={imageStyle}>
				<div className={'ph-image__placeholder ' + (imageLoaded === true ? 'ph-hide' : '')} style={{background: 'url(' + display_image_tiny + ') 50% 50% / cover no-repeat'}} />
				<div className="ph-image__content">
					{children}
				</div>
  		</div>
  	);

  	// return (

  	// 	<div className="ph__wrapper" style={imageStyle}>

			// 	<svg style={{width: '100%', height: '100%'}} xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" class="svgBlur">

			// 	  <filter id="svgBlurFilter">
			// 	    <feGaussianBlur in="SourceGraphic" stdDeviation="10" />
			// 	  </filter>

			// 	  <image xlinkHref={display_image} x="0" y="0" style={{width: '100%', height: '100%'}} preserveAspectRatio="xMinYMin slice" />

			// 	  <image xlinkHref={display_image_tiny} x="0" y="0" style={{width: '100%', height: '100%'}} className={imageLoaded ? 'ph-hide' : ''} filter="url(#svgBlurFilter)" />

			// 	</svg>

			// </div>
  	// )
  }
}

export default ProgressiveImage;



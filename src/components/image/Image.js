import React, { Component } from 'react';
import { getImage } from '../../utils/helpers';
import ProgressiveImage from './ProgressiveImage';
import './image.scss';

class Image extends Component {

  render() {
  	const { url, placeholder, url_mobile, placeholder_mobile, width, height, ratio, window_width } = this.props;
    // return (
    //   <div className="image__wrapper">
    //     <div className="image" style={{backgroundImage: `url(${getImage(url)})`}}></div>
    //     <div className="image__overlay"></div>
    //   </div>
    // );
    return (
      <div className="image__wrapper">
        <ProgressiveImage
          url={getImage(url)}
          placeholder={getImage(placeholder)}
          url_mobile={getImage(url_mobile)}
          placeholder_mobile={getImage(placeholder_mobile)}
          width={width}
          height={height}
          ratio={ratio}
          window_width={window_width}
        />
        <div className="image__overlay"></div>
      </div>
    );
  }
}

export default Image;
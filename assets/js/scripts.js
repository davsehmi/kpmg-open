/*!
 * KPMG Open
 * 
 * 
 * @author AB
 * @version 1.0.0
 * Copyright 2018.
 */


// create a scene
// new ScrollMagic.Scene({
// 		duration: 100,	// the scne should last for a scroll distance of 100px
// 		offset: 0		// start this scene after scrolling for 50px
// 	})
// 	.setPin('.hero__sidebar') // pins the element for the the scene's duration
// 	.addTo(controller); // assign the scene to the controller


/* --- ANIMATIONS --*/

var controller = new ScrollMagic.Controller();


// build tween
var tl_shrink_sidebar = new TimelineMax();
var tl_open_sidebar = new TimelineMax();
var tl_close_sidebar = new TimelineMax();
var tl_hero = new TimelineMax();


// tl_hero.to('.hero__image', 1, { translate: 'translateX(-200px)'}, '0');

// var scene_hero = new ScrollMagic.Scene({
//     triggerElement: '.hero__image',
//     duration: 400,  // the scene should last for a scroll distance of 100px
//     offset: 0  // start this scene after scrolling for 50px
//     })
//   .setTween(tl_hero)
//   // .setPin(".hero") // pins the element for the the scene's duration
//   .addTo(controller); // assign the scene to the controller


// tl_hero
//   .to('.hero__image', 1, {
//    transform: 'translateY(-200px)',
//   }, '0');

// // tweenHideIntro

// // build scene
// var scene0 = new ScrollMagic.Scene({
//   triggerElement: ".hero__image",
//   offset: 100,
// })
//   .setTween(tl_hero)
//   // .addIndicators({name: 'timeline'})
//   .addTo(controller);





tl_shrink_sidebar
  .to('nav.main', 0, {
    autoAlpha: 0
  }, '0')
  .to('nav.main', 0, {
    display: 'none'
  }, '0')
  .to('.logo_p', 0, {
    autoAlpha: 0
  }, '0')
  .to('.logo_e', 0, {
    autoAlpha: 0
  }, '0')
  .to('.logo_n', 0, {
    autoAlpha: 0
  }, '0')
  .to('.sidebar__hero', 0.2, {
   autoAlpha: 0,
  }, '0')
  .to('.sidebar', 0.2, {
    width: '60px',
  }, '0.2')
  .to('.sidebar__nav', 0.2, {
    autoAlpha: 1,
  }, '0.4');


// tweenHideIntro

// build scene
var scene = new ScrollMagic.Scene({
  triggerElement: ".content-wrapper",
  offset: -300,
})
	.setTween(tl_shrink_sidebar)
  // .on('start', function() {
  //   tl_close_sidebar.restart();
  //   is_nav_open = false;
  //   console.log('closed')
  // })
	// .addIndicators({name: 'timeline'})
	.addTo(controller);

var is_nav_open = false

$('.burger').click( function() {
  console.log(is_nav_open);
  if (!is_nav_open) {
    tl_open_sidebar
      .to('nav.main', 0, {
        display: 'block'
      }, '0')
      .to('.sidebar', 0.2, {
        width: '360px'
      }, '0')
      .to('nav.main', 0.2, {
        autoAlpha: 1
      }, '0')
      .to('.logo_p', 0.4, {
        autoAlpha: 1,
        transform: 'translateX(0)'
      }, '0.1')
      .to('.logo_e', 0.4, {
        autoAlpha: 1,
        transform: 'translateX(0)'
      }, '0.2')
      .to('.logo_n', 0.4, {
        autoAlpha: 1,
        transform: 'translateX(0)'
      }, '0.3');
    tl_open_sidebar.restart();
    is_nav_open = true;
  }
  else {
    tl_close_sidebar
      .to('.sidebar', 0.2, {
        width: '60px'
      }, '0')
      .to('nav.main', 0.2, {
        autoAlpha: 0
      }, '0')
      .to('.logo_p', 0.2, {
        autoAlpha: 0
      }, '0.1')
      .to('.logo_e', 0.2, {
        autoAlpha: 0
      }, '0.1')
      .to('.logo_n', 0.2, {
        autoAlpha: 0
      }, '0.1');

    tl_close_sidebar.restart();
    is_nav_open = false;
  }
});




var controller = new ScrollMagic.Controller();

// Single item fade in and slide up...

$(".story").each(function() {

  var tween = TweenMax.from(this, .6, {
          y: 100,
          autoAlpha: 0,
          delay: 0,
          ease: Power2.easeOut
      }, .1);

  var scene1 = new ScrollMagic.Scene({
          triggerElement: this,
          offset: -200,
          reverse: false
      })
    .setTween(tween)
    .addTo(controller)
    // .addIndicators();
});


/* --- INTRO --- */

var tl_intro = new TimelineMax();

function start_intro() {
  tl_intro
    .to('.intro__loader', 0.5, { autoAlpha: 0 }, '0')
    .to('.intro__loader', 0, { display: 'none' }, '0.3')
    .to('.intro__titles', 0.3, { display: 'block' }, '0.3')
    .to('.intro__title', 0.6, { autoAlpha: 1 }, '0.3')
    .to('.intro__subtitle span.1', 0.2, { autoAlpha: 1 }, '0.9')
    .to('.intro__subtitle span.2', 0.2, { autoAlpha: 1 }, '1.3')
    .to('.intro__subtitle span.3', 0.2, { autoAlpha: 1 }, '1.7')

    .to('.intro__transition--1', 0.4, { transform: 'scaleX(1)' }, '2.6')
    .to('.intro__transition--1', 0, { transformOrigin: '100% 100%' }, '3')
    .to('.intro__transition--1', 0.4, { transform: 'scaleX(0)' }, '3')

    .to('.intro__transition--2', 0.4, { transform: 'scaleX(1)' }, '3')
    .to('.intro__transition--2', 0, { transformOrigin: '100% 100%' }, '3.4')
    .to('.intro__transition--2', 0.4, { transform: 'scaleX(0)' }, '3.4')

    .to('.intro__titles', 0, { autoAlpha: 0 }, '3.4')
    .to('.intro-wrapper', 0, { background: 'transparent' }, '3.4')
    .to('.hero', 0, { display: 'block' }, '3.4')
    // .fromTo('.hero__image', 0.8, { transform: 'scale(1.1)' }, { transform: 'scale(1)' }, '3.4')

    .to('.sidebar', 0.2, { transform: 'scaleX(1)' }, '3.4')

    .to('.sidebar__header', 0.6, { autoAlpha: 1 }, '4')
    .to('.sidebar__content', 0.6, { autoAlpha: 1 }, '4')

    // .to('body', 0, { overflowY: 'initial' }, '4');
}

// setTimeout(start_intro, 2000);

var intro_image = new Image();

// we will call this function when the image has finished loading
function notify_complete() {
  start_intro();
}

function load_image() {
  // call the notify_complete function when the image has loaded
  intro_image.onload = notify_complete;

  // load the image
  intro_image.src = 'assets/img/hero_01.jpg';
}

load_image();

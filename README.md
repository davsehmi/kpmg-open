# KPMG LIFE

This project was bootstrapped with an ejected version of [Create React App](https://github.com/facebook/create-react-app) and uses [Yarn](https://yarnpkg.com/lang/en/docs/install/#mac-stable) as a package manager

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

Also concurrently starts the proxy server found in  `server/server.js` which runs on [http://localhost:3001](http://localhost:3001) and is configured in the `package.json`:

```
{
  "name": "kpmg-open",
  "proxy": "http://localhost:3001",
  ...
}
```

### `yarn staging`

Builds the app for staging to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

Note: ensure that the hompage value in the `package.json` file set as a relative value as below:
```
{
  "name": "kpmg-open",
  "version": "0.1.0",
  "private": true,
  "homepage": "./",
  ...
}
```

### `yarn upload-staging`

Builds the app for staging and uploads to the staging server.

### `yarn build`

Builds the app for build to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

Note: ensure that the hompage value in the `package.json` file is an absoute value as below:
```
{
  "name": "kpmg-open",
  "version": "0.1.0",
  "private": true,
  "homepage": "https://portal.kpmg.co.uk/kpmglife/onlinemagazine",
  ...
}
```